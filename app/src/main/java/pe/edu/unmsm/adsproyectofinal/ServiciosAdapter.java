package pe.edu.unmsm.adsproyectofinal;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

import pe.edu.unmsm.adsproyectofinal.DTO.ServiciosDTO;

/**
 * Created by JesusMatosPortocarre on 28/01/2018.
 */

public class ServiciosAdapter extends ArrayAdapter<ServiciosDTO> {


    public ServiciosAdapter(Context context, List<ServiciosDTO> lista) {
        super(context, 0, lista);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        //Obtener el Inflater
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(
                Context.LAYOUT_INFLATER_SERVICE );
        if(null == convertView){
            convertView = inflater.inflate(
                    R.layout.layout_item_servicio,
                    parent,
                    false
            );
        }
        // Referencias UI
        TextView origen = (TextView) convertView.findViewById(R.id.tvOrigen);
        TextView destino = (TextView) convertView.findViewById(R.id.tvDestino);
        TextView fecha = (TextView) convertView.findViewById(R.id.tvFecha);
        TextView hora = (TextView) convertView.findViewById(R.id.tvHora);
        ImageButton boton = (ImageButton) convertView.findViewById(R.id.btnIrWaze);

        ServiciosDTO servicio = getItem(position);

        // Setup
        origen.setText(servicio.getOrigen());
        destino.setText(servicio.getDestino());
        fecha.setText(servicio.getFechaIngreso());
        hora.setText(servicio.getHoraIngreso());

        return convertView;
    }


}
