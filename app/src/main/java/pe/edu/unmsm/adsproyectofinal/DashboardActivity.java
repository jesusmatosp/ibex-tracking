package pe.edu.unmsm.adsproyectofinal;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class DashboardActivity extends AppCompatActivity {

    protected TextView lblNombresCompletos;
    protected String nombreUsuario;
    protected String apellidosUsuario;
    protected Integer codigoUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        lblNombresCompletos = (TextView) findViewById(R.id.tvUsuario);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        nombreUsuario = bundle.getString("nombres");
        apellidosUsuario = bundle.getString("apellidos");
        codigoUsuario = bundle.getInt("codusuario");

        lblNombresCompletos.setText("Bienvenido:" + bundle.getString("nombres") +" " + bundle.getString("apellidos"));

    }

    public void irServicios(View view){
        Intent intent = new Intent(getApplicationContext(), ServiciosActivity.class);
        intent.putExtra("nombres", nombreUsuario);
        intent.putExtra("apellidos", apellidosUsuario);
        intent.putExtra("codusuario", codigoUsuario);
        startActivity(intent);
    }

    public void irIniciarCarrera(View view){
        Intent intent = new Intent(getApplicationContext(), LocationADS.class);
        intent.putExtra("nombres", nombreUsuario);
        intent.putExtra("apellidos", apellidosUsuario);
        startActivity(intent);
    }
}
