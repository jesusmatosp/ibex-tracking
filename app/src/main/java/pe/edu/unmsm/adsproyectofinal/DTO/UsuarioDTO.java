package pe.edu.unmsm.adsproyectofinal.DTO;

import android.content.Intent;

import java.io.Serializable;

/**
 * Created by jesus on 15/07/2017.
 */
public class UsuarioDTO implements Serializable {

    private Integer id;
    private String username;
    private String password;
    private String nombres;
    private String apellidos;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }
}
