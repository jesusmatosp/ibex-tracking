package pe.edu.unmsm.adsproyectofinal;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.gms.common.api.GoogleApiClient;

import org.w3c.dom.Text;

import pe.edu.unmsm.adsproyectofinal.DTO.CoordenadasDTO;
import pe.edu.unmsm.adsproyectofinal.servicios.ClienteRegistraServicio;


public class LocationADS extends AppCompatActivity {

    private static final String LOGTAG = "android-localizacion";
    private static final int PETICION_PERMISO_LOCALIZACION = 101;
    private GoogleApiClient apiClient;
    private CoordenadasDTO coordenadasDTO;
    private TextView lblLatitud;
    private TextView lblLongitud;
    private EditText txtCodServicio;
    private ToggleButton btnActualizar;
    private Integer codigoServicio;
    private TextView lblNombresCompletos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        lblLatitud = (TextView) findViewById(R.id.v_id_latitud);
        lblLongitud = (TextView) findViewById(R.id.v_id_longitud);
        btnActualizar = (ToggleButton) findViewById(R.id.btnActualizar);
        txtCodServicio = (EditText) findViewById(R.id.txtCodServicio);
        lblNombresCompletos = (TextView) findViewById(R.id.lbl_nombres_usuarios);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        lblNombresCompletos.setText(bundle.getString("nombres") +" " + bundle.getString("apellidos"));

        if(validarInput()){
            btnActualizar.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    String message = "";
                    if (isChecked) {
                        message = "Se activa la Transferencia de Coordenadas";
                        updateLocation();
                    } else {
                        message = "Se inactiva la Transferencia de Coordenadas";
                    }
                    Toast.makeText(getApplicationContext(), message,
                            Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    public boolean validarInput(){
        if(txtCodServicio == null){
            Toast.makeText(getApplicationContext(), "Debe ingresar un valor como código del Servicio", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public void updateUI(android.location.Location loc) {
        if (loc != null) {
            coordenadasDTO = new CoordenadasDTO();
            lblLatitud.setText(String.valueOf(loc.getLatitude()));
            lblLongitud.setText(String.valueOf(loc.getLongitude()));
            coordenadasDTO.setLatitud(lblLatitud.getText().toString());
            coordenadasDTO.setLongitud(lblLongitud.getText().toString());
            codigoServicio = Integer.parseInt(txtCodServicio.getText().toString());
            Log.i(LOGTAG, "Cod Servicio: "+txtCodServicio.getText().toString());
            coordenadasDTO.setCodServicio(codigoServicio);
            transfiereServer(coordenadasDTO);
        } else {
            lblLatitud.setText(" (desconocida) ");
            lblLongitud.setText(" (desconocida) ");
        }
    }

    public void transfiereServer(CoordenadasDTO coordenadasDTO){
        ClienteRegistraServicio clienteRegistraServicio = new ClienteRegistraServicio();
        clienteRegistraServicio.registrarDatos(getApplicationContext(), coordenadasDTO);
    }

    public void updateLocation(){
        long intervaloTransferencia = 5000L;
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        LocationListener locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                updateUI(location);
            }
            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {}
            @Override
            public void onProviderEnabled(String provider) {}
            @Override
            public void onProviderDisabled(String provider) {}
        };
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast toast = Toast.makeText(getApplicationContext(), "Error No cuenta con los permisos necesarios", Toast.LENGTH_LONG);
            toast.show();
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, intervaloTransferencia, 0, locationListener);
    }
    }
