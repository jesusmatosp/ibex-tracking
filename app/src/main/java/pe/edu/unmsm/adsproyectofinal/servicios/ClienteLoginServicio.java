package pe.edu.unmsm.adsproyectofinal.servicios;

import android.content.Context;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import pe.edu.unmsm.adsproyectofinal.DTO.ResponseLoginDTO;
import pe.edu.unmsm.adsproyectofinal.DTO.UsuarioDTO;

/**
 * Created by jesus on 15/07/2017.
 */
public class ClienteLoginServicio {

    String SERVER_NAME = "http://servicio-taxi-jmatos.cfapps.io";
    String REST_SERVICE = SERVER_NAME + "/login";
    String TYPE = "application/json";
    private static final String LOGTAG = "android-localizacion";
    private UsuarioDTO user;

    public ResponseLoginDTO login(Context context, UsuarioDTO usuarioDTO){
        SyncHttpClient cliente = new SyncHttpClient();
        try {
            JSONObject jsonParams = new JSONObject();
            jsonParams.put("username", usuarioDTO.getUsername());
            jsonParams.put("password", usuarioDTO.getPassword());

            StringEntity entity = new StringEntity(jsonParams.toString());
            cliente.post(context, REST_SERVICE,entity, TYPE,
                    new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            if(statusCode == 200){
                                String resultado = new String(responseBody);
                                obtenerDatosJSON(resultado);
                                Log.i(LOGTAG, "Respuesta: " + resultado);
                            }
                        }
                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            Log.e(LOGTAG, error.getLocalizedMessage());
                        }
                    });
        }catch (Exception ex){
            ex.printStackTrace();
        }

        ResponseLoginDTO responseLoginDTO = new ResponseLoginDTO();
        if(user!=null){
            responseLoginDTO.setUsuarioDTO(user);
            responseLoginDTO.setEstado(1);
            responseLoginDTO.setMensaje("OK");
        }else{
            responseLoginDTO.setEstado(2);
            responseLoginDTO.setMensaje("Usuario o Contraseña inválidas");
        }
        return responseLoginDTO;
    }

    public void obtenerDatosJSON(String response){
        try{
            JSONObject respuestaJSON = new JSONObject(response).getJSONObject("account");
            if(respuestaJSON!=null){
                UsuarioDTO usuarioDTO = new UsuarioDTO();
                usuarioDTO.setId(respuestaJSON.getInt("codusuario"));
                usuarioDTO.setNombres(respuestaJSON.getString("nombres"));
                usuarioDTO.setApellidos(respuestaJSON.getString("apellidos"));
                usuarioDTO.setUsername(respuestaJSON.getString("username"));
                setUser(usuarioDTO);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public UsuarioDTO getUser() {
        return user;
    }

    public void setUser(UsuarioDTO user) {
        this.user = user;
    }
}
