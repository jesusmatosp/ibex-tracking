package pe.edu.unmsm.adsproyectofinal.servicios;

import android.content.Context;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import pe.edu.unmsm.adsproyectofinal.DTO.ServiciosDTO;
import pe.edu.unmsm.adsproyectofinal.DTO.UsuarioDTO;

/**
 * Created by JesusMatosPortocarre on 28/01/2018.
 */

public class ClienteServicioTransporteServicio {

    String SERVER_NAME = "http://servicio-taxi-jmatos.cfapps.io";
    String REST_SERVICE = SERVER_NAME + "/servicios";
    String TYPE = "application/json";
    private static final String LOGTAG = "android-localizacion";

    final List<ServiciosDTO> lstServicios = new ArrayList<>();

    public List<ServiciosDTO> listarServicios(Context context, Integer codusuario){
        AsyncHttpClient cliente = new AsyncHttpClient();

        String url = REST_SERVICE + "/"+codusuario;
        cliente.get(context, url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if(statusCode == 200){
                    String resultado = new String(responseBody);
                    try {
                        JSONArray jsonarray = new JSONObject(resultado).getJSONArray("servicios");
                        Log.i("Cantidad", ">>>>" + String.valueOf(jsonarray.length()));
                    } catch (JSONException e) {
                        Log.e("ERRORR", e.getLocalizedMessage());
                    }
                    obtenerDatosJSON(resultado);
                    Log.i(LOGTAG, "Respuesta: "+resultado);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.e(LOGTAG, error.getLocalizedMessage());
            }
        });

        return lstServicios;
    }

    public void obtenerDatosJSON(String response){
        try{
            JSONArray jsonarray = new JSONObject(response).getJSONArray("servicios");
            //Log.i("json", "" + jsonarray.length());
            for (int i = 0; i < jsonarray.length(); i++) {
                JSONObject jsonobject = jsonarray.getJSONObject(i);
                ServiciosDTO serviciosDTO = new ServiciosDTO(
                        jsonobject.getInt("idServicio"),
                        jsonobject.getString("origen"),
                        jsonobject.getString("destino"),
                        jsonobject.getString("horaIngreso"),
                        jsonobject.getString("fechaServicio"),
                        jsonobject.getInt("codusuario") );
                lstServicios.add(serviciosDTO);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
