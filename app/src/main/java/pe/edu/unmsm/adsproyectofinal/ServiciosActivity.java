package pe.edu.unmsm.adsproyectofinal;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import pe.edu.unmsm.adsproyectofinal.DTO.ServiciosDTO;
import pe.edu.unmsm.adsproyectofinal.servicios.ClienteServicioTransporteServicio;

public class ServiciosActivity extends AppCompatActivity {

    private ListView lista = null;
    private Integer codigoUsuario = 0;

    private ProgressBar progressBar;

    String SERVER_NAME = "http://servicio-taxi-jmatos.cfapps.io";
    String REST_SERVICE = SERVER_NAME + "/servicios";
    String TYPE = "application/json";
    private static final String LOGTAG = "android-localizacion";
    final List<ServiciosDTO> lstServicios = new ArrayList<>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_servicios);

        // Instanciar Lista
        lista = (ListView) findViewById(R.id.listaServicios);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        codigoUsuario = bundle.getInt("codusuario");
        progressBar = (ProgressBar) findViewById(R.id.loading_servicios);

        progressBar.setVisibility(View.VISIBLE);
        listarServicios(this, codigoUsuario);
    }
    public List<ServiciosDTO> listarServicios(Context context, Integer codusuario){
        AsyncHttpClient cliente = new AsyncHttpClient();

        String url = REST_SERVICE + "/"+codusuario;
        cliente.get(context, url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if(statusCode == 200){
                    String resultado = new String(responseBody);
                    Log.i(LOGTAG, "Respuesta: "+resultado);
                    cargarLista(obtenerDatosJSON(new String(responseBody)));
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.e(LOGTAG, error.getLocalizedMessage());
            }
        });

        return lstServicios;
    }

    public ArrayList<ServiciosDTO> obtenerDatosJSON(String response){
        ArrayList<ServiciosDTO> lista = new ArrayList<>();
        try{
            JSONArray jsonarray = new JSONObject(response).getJSONArray("servicios");
            for (int i = 0; i < jsonarray.length(); i++) {
                JSONObject jsonobject = jsonarray.getJSONObject(i);
                ServiciosDTO serviciosDTO = new ServiciosDTO(
                        jsonobject.getInt("idServicio"),
                        jsonobject.getString("origen"),
                        jsonobject.getString("destino"),
                        jsonobject.getString("horaIngreso"),
                        jsonobject.getString("fechaServicio"),
                        jsonobject.getInt("codusuario") );
                lista.add(serviciosDTO);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return lista;
    }

    public void cargarLista(ArrayList<ServiciosDTO> datos){
        // Iniciar el adaptardor con la fuente de datos:
        ServiciosAdapter serviciosAdapter = new ServiciosAdapter(getApplicationContext(), datos);
        // Relacionando la lista con el adaptador.
        lista.setAdapter(serviciosAdapter);

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Log.i("Hola", "Click "+position);

                Uri gmmIntentUri = Uri.parse("google.streetview:cbll=46.414382,10.013988");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            }
        });

        lista.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.i("pryueba", "posición: "+position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void init(){
        // Inicializar Adaptador
        List<ServiciosDTO> listaServicios = new ArrayList<>();
        // Recupera Lista Servicio Web

        // Fin Recupera
        ClienteServicioTransporteServicio cliente = new ClienteServicioTransporteServicio();
        listaServicios = cliente.listarServicios(getApplicationContext(), codigoUsuario);
        for(ServiciosDTO s:listaServicios){
            Log.i("demo", s.getOrigen());
        }
        // Iniciar el adaptardor con la fuente de datos:
        ServiciosAdapter serviciosAdapter = new ServiciosAdapter(getApplicationContext(), listaServicios);

        // Relacionando la lista con el adaptador.
        lista.setAdapter(serviciosAdapter);
    }
}
