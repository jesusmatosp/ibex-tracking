package pe.edu.unmsm.adsproyectofinal.servicios;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import pe.edu.unmsm.adsproyectofinal.DTO.CoordenadasDTO;

/**
 * Created by jesus on 15/07/2017.
 */
public class ClienteRegistraServicio {

    String SERVER_NAME = "http://servicio-taxi-jmatos.cfapps.io/";
    String REST_SERVICE = SERVER_NAME + "coordenadas/add";
    String TYPE = "application/json";
    private static final String LOGTAG = "android-localizacion";

    public void registrarDatos(final Context context, CoordenadasDTO coordenadasDTO){
        AsyncHttpClient cliente = new AsyncHttpClient();
        try {

            JSONObject jsonParams = new JSONObject();
            jsonParams.put("longitud", coordenadasDTO.getLongitud());
            jsonParams.put("latitud", coordenadasDTO.getLatitud());
            jsonParams.put("codservicio", coordenadasDTO.getCodServicio());

            StringEntity entity = new StringEntity(jsonParams.toString());
            cliente.post(context, REST_SERVICE,entity, TYPE,
                    new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            if(statusCode == 200){
                                String resultado = new String(responseBody);
                                Log.i(LOGTAG, "Respuesta: "+resultado);
                            }
                        }
                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            Log.e(LOGTAG, error.getLocalizedMessage());
                        }
                    });
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }

}
