package pe.edu.unmsm.adsproyectofinal.DTO;

import java.io.Serializable;

/**
 * Created by jesus on 15/07/2017.
 */
public class CoordenadasDTO implements Serializable {

    private String longitud;
    private String latitud;
    private Integer codServicio;
    private Integer id;

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public Integer getCodServicio() {
        return codServicio;
    }

    public void setCodServicio(Integer codServicio) {
        this.codServicio = codServicio;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}


