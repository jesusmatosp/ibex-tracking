package pe.edu.unmsm.adsproyectofinal;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import pe.edu.unmsm.adsproyectofinal.DTO.CoordenadasDTO;
import pe.edu.unmsm.adsproyectofinal.servicios.ClienteRegistraServicio;

public class Localiazcion extends AppCompatActivity {

    Button btnAbrirLocalizacion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_localiazcion);

        btnAbrirLocalizacion = (Button) findViewById(R.id.btnLoadGPS);
        btnAbrirLocalizacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intento = new Intent(getApplicationContext(), LocationADS.class);
                startActivity(intento);

                /*CoordenadasDTO coordenadasDTO = new CoordenadasDTO();
                coordenadasDTO.setCodServicio(25);
                coordenadasDTO.setLatitud("61.9667");
                coordenadasDTO.setLongitud("-11.5333");
                ClienteRegistraServicio clienteRegistraServicio = new ClienteRegistraServicio();
                clienteRegistraServicio.registrarDatos(getApplicationContext(), coordenadasDTO);*/

            }
        });


    }
}
