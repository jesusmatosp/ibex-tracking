package pe.edu.unmsm.adsproyectofinal.DTO;

import java.io.Serializable;

/**
 * Created by JesusMatosPortocarre on 28/01/2018.
 */

public class ServiciosDTO implements Serializable {

    private Integer idServicio;
    private String origen;
    private String destino;
    private String horaIngreso;
    private String fechaIngreso;
    private Integer codusuario;

    public ServiciosDTO(Integer idServicio, String origen, String destino, String horaIngreso, String fechaIngreso, Integer codusuario) {
        this.idServicio = idServicio;
        this.origen = origen;
        this.destino = destino;
        this.horaIngreso = horaIngreso;
        this.fechaIngreso = fechaIngreso;
        this.codusuario = codusuario;
    }

    public Integer getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(Integer idServicio) {
        this.idServicio = idServicio;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getHoraIngreso() {
        return horaIngreso;
    }

    public void setHoraIngreso(String horaIngreso) {
        this.horaIngreso = horaIngreso;
    }

    public String getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public Integer getCodusuario() {
        return codusuario;
    }

    public void setCodusuario(Integer codusuario) {
        this.codusuario = codusuario;
    }
}
