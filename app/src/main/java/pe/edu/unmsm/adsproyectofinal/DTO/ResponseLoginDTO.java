package pe.edu.unmsm.adsproyectofinal.DTO;

import java.io.Serializable;

/**
 * Created by jesus on 15/07/2017.
 */
public class ResponseLoginDTO implements Serializable {

    private Integer estado;
    private String mensaje;
    private UsuarioDTO usuarioDTO;

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public UsuarioDTO getUsuarioDTO() {
        return usuarioDTO;
    }

    public void setUsuarioDTO(UsuarioDTO usuarioDTO) {
        this.usuarioDTO = usuarioDTO;
    }
}
